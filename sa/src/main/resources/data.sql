-- users SENHA admin
INSERT INTO users (id, name, username, email, password, active) values (1, 'Administrador', 'admin' , 'admin@admin.com', '$2a$10$3y4RPg/.MZOCz9bsKoHtbOWmBlg4L43rLyU1ee/r0UeOg0Sa5SvzO', true);
INSERT INTO users (id, name, username, email, password, active) values (2, 'Usuario', 'user' , 'user@user.com', '$2a$10$3y4RPg/.MZOCz9bsKoHtbOWmBlg4L43rLyU1ee/r0UeOg0Sa5SvzO', true);

-- roles
INSERT INTO role (id, name, active) values (1, 'ROLE_INCLUIR_COLABORADOR', true);
INSERT INTO role (id, name, active) values (2, 'ROLE_ALTERAR_COLABORADOR', true);
INSERT INTO role (id, name, active) values (3, 'ROLE_EXCLUIR_COLABORADOR', true);
INSERT INTO role (id, name, active) values (4, 'ROLE_PESQUISAR_COLABORADOR', true);
INSERT INTO role (id, name, active) values (5, 'ROLE_LISTAR_COLABORADOR', true);
INSERT INTO role (id, name, active) values (6, 'ROLE_REINDEX_COLABORADOR', true);

-- profile
INSERT INTO profile (id, name, active) values (1, 'Administrador', true);
INSERT INTO profile (id, name, active) values (2, 'Usuario', true);

-- profiles_roles ADMIN
INSERT INTO profiles_roles (id_profile, id_role) values (1,1);
INSERT INTO profiles_roles (id_profile, id_role) values (1,2);
INSERT INTO profiles_roles (id_profile, id_role) values (1,3);
INSERT INTO profiles_roles (id_profile, id_role) values (1,4);
INSERT INTO profiles_roles (id_profile, id_role) values (1,5);
INSERT INTO profiles_roles (id_profile, id_role) values (1,6);


-- profiles_roles
INSERT INTO profiles_roles (id_profile, id_role) values (2,1);
INSERT INTO profiles_roles (id_profile, id_role) values (2,2);

-- user_profile
INSERT INTO user_profile (id_user, id_profile) values (1,1);
INSERT INTO user_profile (id_user, id_profile) values (2,2);