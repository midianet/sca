package brothers.sca.resource;

import brothers.sca.domain.Role;
import brothers.sca.repository.RoleRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@Log4j2
@RestController
@RequestMapping(value = "/api/roles", path = "/api/roles")
public class RoleResource {

    @Autowired
    private RoleRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public Role post(@RequestBody Role role, HttpServletResponse response) {
        log.trace("Create role: {role}", role);
        role.setId(null);
        role = repository.save(role);
        response.addHeader(HttpHeaders.LOCATION,
                ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(role.getId())
                        .toUri().toString() + role.getId());
        return role;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void put(@PathVariable Long id, @RequestBody Role role) {
        log.trace("Update role {}", role);
        var persistent = findById(id);
        BeanUtils.copyProperties(role, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void delete(@PathVariable Long id) {
        log.trace("Delete role {id}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping("/all")
    //@PreAuthorize("hasAuthority('ROLE_READ')")
    public Collection<Role> list() {
        log.trace("List All role");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    //@PreAuthorize("hasAuthority('ROLE_READ')")
    public Role findById(@PathVariable Long id) {
        log.trace("Find role by id: {id}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Role %d", id)));
    }

}