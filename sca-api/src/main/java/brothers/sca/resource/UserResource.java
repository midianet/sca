package brothers.sca.resource;

import brothers.sca.domain.User;
import brothers.sca.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@Log4j2
@RestController
@RequestMapping(value = "/api/users", path = "/api/users")
public class UserResource {

    @Autowired
    private UserRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    //@PreAuthorize("hasAuthority('USER_ADMIN')")
    public User post(@RequestBody User user, HttpServletResponse response) {
        log.trace("Create user: {}", user);
        user.setId(null);
        user = repository.save(user);
        response.addHeader(HttpHeaders.LOCATION,
                ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(user.getId())
                        .toUri().toString() + user.getId());
        return user;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    //@PreAuthorize("hasAuthority('USER_ADMIN')")
    public void put(@PathVariable Long id, @RequestBody User user) {
        log.trace("Update user: {}", user);
        var persistent = findById(id);
        BeanUtils.copyProperties(user, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@PreAuthorize("hasAuthority('USER_ADMIN')")
    public void delete(@PathVariable Long id) {
        log.trace("Delete user: {}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping("/all")
    //@PreAuthorize("hasAuthority('USER_READ')")
    public Collection<User> list() {
        log.trace("List all user");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    //@PreAuthorize("hasAuthority('USER_READ')")
    public User findById(@PathVariable Long id) {
        log.trace("Find user by id: {}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("User %d", id)));
    }

}