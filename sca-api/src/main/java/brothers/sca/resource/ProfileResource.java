package brothers.sca.resource;

import brothers.sca.domain.Profile;
import brothers.sca.repository.ProfileRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@Log4j2
@RestController
@RequestMapping(value = "/api/profiles", path = "/api/profiles")
public class ProfileResource {

    @Autowired
    private ProfileRepository repository;

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    //@PreAuthorize("hasAuthority('PROFILE_ADMIN')")
    public Profile post(@RequestBody Profile profile, HttpServletResponse response) {
        log.trace("Create profile: {role}", profile);
        profile.setId(null);
        profile = repository.save(profile);
        response.addHeader(HttpHeaders.LOCATION,
                ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(profile.getId())
                        .toUri().toString() + profile.getId());
        return profile;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    //@PreAuthorize("hasAuthority('PROFILE_ADMIN')")
    public void put(@PathVariable Long id, @RequestBody Profile profile) {
        log.trace("Update profile {}", profile);
        var persistent = findById(id);
        BeanUtils.copyProperties(profile, persistent, "id");
        repository.save(persistent);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@PreAuthorize("hasAuthority('PROFILE_ADMIN')")
    public void delete(@PathVariable Long id) {
        log.trace("Delete profile {id}", id);
        findById(id);
        repository.deleteById(id);
    }

    @GetMapping("/all")
    //@PreAuthorize("hasAuthority('PROFILE_READ')")
    public Collection<Profile> list() {
        log.trace("List All profile");
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    //@PreAuthorize("hasAuthority('PROFILE_READ')")
    public Profile findById(@PathVariable Long id) {
        log.trace("Find profile by id: {id}", id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Profile %d", id)));
    }

}
