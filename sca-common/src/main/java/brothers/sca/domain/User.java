package brothers.sca.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 20, nullable = false, unique = true)
    private String username;

    @NotNull
    @Column(length = 80, nullable = false)
    private String name;

    @Email
    @NotNull
    @Column(length = 50, nullable = false, unique = true)
    private String email;

    @NotNull
    @NotEmpty
    @Column(length = 100, nullable = false)
    private String password;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean active;

    @NotEmpty
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_profile", joinColumns = @JoinColumn(name = "id_user")
            , inverseJoinColumns = @JoinColumn(name = "id_profile"))
    private Set<Profile> profiles;

}
