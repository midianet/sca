package brothers.sca.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 80, nullable = false)
    private String name;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean active;

    @NotEmpty
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "profiles_roles", joinColumns = @JoinColumn(name = "id_profile")
            , inverseJoinColumns = @JoinColumn(name = "id_role"))
    private Set<Role> roles;

}
